
Platnosci.pl is the system addressed to all of an eCommerce sites that for their activity require professional payment solutions provider. Platnosci.pl it's quick, safe and easy method of payment for goods and services offered on the Internet so that customers feel complete comfort and convenience.
Platnosci.pl is a polish online payment gateway which integrates all the most popular online bank transfers, provides a credit card payment as well as old fashion bank transfer option.

Dependencies
------------

 * Ubercart

This module is an integration of Platnosci.pl with Drupal eCommerce solution - Ubercart.


Installation
------------

1) Copy the filefield folder to the modules folder in your installation.

2) Enable the module using Administer -> Site building -> Modules.

3) Enable Platnosci.pl payment gateway using Administer -> Store administration -> Configuration -> Payment settings -> Payment gateways.

4) Setup the Platnosci.pl account in the 'Account settings' section under the 'Payment gateways' section. You must provide POS ID, Payment authorization key (pos_auth_key), Primary key, Secondary key parameters. All of them will be given by Platnosci.pl when configuring the Payment Point.

5) Copy the gateway online addresses from the 'Account settings' section: Positive URL, Error URL, Online URL and paste them into the appropriate fields in Payment point configuration section in Platnosci.pl.

6) Enable the Platnosci.pl payment method using Administer -> Store administration -> Configuration -> Payment settings -> Payment methods.

7) Under the same section enable the payment methods you want to be accessible for the user.


Messages
--------

Using Administer -> Store administration -> Configuration -> Payment settings -> Payment gateways you can provide your own info and error messages according to the Platnosci.pl error codes.

