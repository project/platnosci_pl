<?php

/**
 * Implementation of platnosci_pl_payment_error
 * Menu callback function, callback for negative url in platnosci.pl
 *
 */
function platnosci_pl_payment_error(){
  $args = func_get_args();
  list($trans_id, $pos_id, $pay_type, $sess_id, $amount, $order_id, $error_code) = $args;
  $sid = array_shift(explode('_', $sess_id));
  $session = _platnosci_pl_sess_read($sid);
  if($trans_id == 0){
    $order = uc_order_load($order_id);
    if($order->order_status != uc_order_state_default('canceled')){
      uc_order_update_status($order_id, uc_order_state_default('canceled'));
      $message = t('!error_message', array('!error_message' => platnosci_pl_get_error_message($error_code)));
      uc_order_comment_save($order_id, $order->uid, $message, 'order', uc_order_state_default('canceled'), true);
      
      $message = t('Error while processing payment, code: %code', array('%code' => $error_code));
      uc_order_comment_save($order_id, 1, $message, 'admin', uc_order_state_default('canceled'), false);
    }
  }
  return theme('payment_error_page', $error_code);
}

/**
 * Implementation of platnosci_pl_payment_complete
 * Menu callback function, callback for positive url in platnosci.pl
 * Generates message for user after order completion
 *
 */
function platnosci_pl_payment_complete(){
  global $user;
  
  list($trans_id, $pos_id, $pay_type, $sess_id, $amount, $order_id) = func_get_args();
  list($sid, ) = explode('_', $sess_id);
  
  if($order = uc_order_load($order_id)){
  
    if($sid){
      _platnosci_pl_sess_read($sid);
    }
    
    $message_type = $_SESSION['message_type'];
    $output = '<p>'. check_markup(variable_get('uc_msg_order_submit', uc_get_message('completion_message')),
                         variable_get('uc_msg_order_submit_format', FILTER_FORMAT_DEFAULT), FALSE) .'</p>';
    $show_message = check_markup(variable_get('uc_msg_order_'. $message_type, uc_get_message('completion_'. $message_type)), variable_get('uc_msg_order_'.      $message_type .'_format', FILTER_FORMAT_DEFAULT), FALSE);

  if ($show_message != '') {
    $variables['!new_username'] = check_plain($_SESSION['new_user']['name']);
    $variables['!new_password'] = check_plain($_SESSION['new_user']['pass']);
    $output .= '<p>'. strtr($show_message, $variables) .'</p>';
  }
  $output .= '<p>'. check_markup(variable_get('uc_msg_continue_shopping', uc_get_message('continue_shopping')),
                      variable_get('uc_msg_continue_shopping_format', FILTER_FORMAT_DEFAULT),
                      FALSE) .'</p>';

  $output = token_replace_multiple($output, array('global' => NULL, 'order' => $order));
  unset($_SESSION['new_user'], $_SESSION['message_type']);

    return $output;
  }else{
    drupal_goto('cart');
  }
}

/**
 * Implementation of platnosci_pl_online
 * Menu callback, callback for requests form platnosci.pl
 *
 */
function platnosci_pl_online(){
  $pos_id = variable_get('platnosci_pl_pos_id', '') == $_POST['pos_id'] ? $_POST['pos_id'] : 0;
  $sess_id = $_POST['session_id'];
  $ts = $_POST['ts'];
  $sig = $_POST['sig'];
  
  if(_platnosci_pl_validate_sig($sig, $pos_id, $sess_id, $ts, variable_get('platnosci_pl_key2',''))){
    $ts = time();
    $data = array(
      'pos_id' => $pos_id,
      'session_id' => $sess_id,
      'ts' => $ts,
      'sig' => md5($pos_id.$sess_id.$ts.variable_get('platnosci_pl_key1', ''))
    );
    $url = _platnosci_pl_create_url('Payment/get');
    $response = platnosci_pl_request($url, $data);
    
    if($response['response'][0]['status'] == 'OK'){
      $trans = $response['response'][0]['trans'][1];
      platnosci_pl_payment_update($trans);
      print "OK";
//    }else if($response['response'][0]['status'] == 'ERROR'){
    }else{
      $code = $response['response'][0]['error'][1]['nr'];
      watchdog('platnosci pl', 'Transaction error, code: %code, message: %message', array('%code' => $code, '%message' => _platnosci_pl_getErrorDesc($code)), WATCHDOG_ERROR);
    }
  }else{
    watchdog('platnosci pl', 'Error while receiving message from Platnosci.pl - signature does not match', array(),  WATCHDOG_ERROR);
  }
  exit;
}

/**
 * Implementation of platnosci_pl_payment_update
 * Setting apropriate status for the order
 *
 */
function platnosci_pl_payment_update($transaction){

  if(_platnosci_pl_validate_sig($transaction['sig'], variable_get('platnosci_pl_pos_id', 0), $transaction['session_id'], $transaction['order_id'], $transaction['status'], $transaction['amount'], $transaction['desc'], $transaction['ts'], variable_get('platnosci_pl_key2', 0))){
    
    $order = uc_order_load($transaction['order_id']);
    
    if($order){
      if($order->payment_details['ppl_status'] != PPTS_FINALIZED && uc_payment_balance($order) > 0){
        if($transaction['status'] == PPTS_FINALIZED){
            $comment = t('Platnosci.pl transaction ID: !transactionid', array('!transactionid' => $transaction['id']));
            uc_payment_enter($order->order_id, $order->payment_method, $transaction['amount']/100, $order->uid ? $order->uid : 0, $transaction['id'], $comment);

            $message = t('<b>Transaction processed successfuly through Platnosci.pl - amount: %amount', array('%amount' => uc_currency_format($transaction['amount']/100)));
                        
            $user_message = t('Transaction processed successfuly - transaction id: %id, amount: %amount', array('%amount' => uc_currency_format($transaction['amount']/100), '%id' => $transaction['id']));
            uc_order_comment_save($order->order_id, $order->uid, $user_message, 'order', 'payment_received', true);
            
            if(!$order->payment_details['trans_id']){
              $order->payment_details['trans_id'] = $transaction['id'];
            }
            $user = user_load(array('uid' => $order->uid));
//            ca_pull_trigger('uc_checkout_complete', $order, $user);
            platnosci_pl_extract_payment_data($transaction, $order);
        }
        else if($transaction['status'] == PPTS_PENDING){
          $message = t('<b>Transaction is pending</b> - transaction id: %id, amount: %amount', array('%amount' => uc_currency_format($transaction['amount']/100), '%id' => $transaction['id']));
          
          if(uc_order_status_data($order->order_status, 'state') != 'payment_pending'){
            uc_order_update_status($order->order_id, uc_order_state_default('payment_pending'));
          }
        }
        else if($transaction['status'] == PPTS_REJECTED){
          $message = t('<b>Transaction has been rejected by Płatnosci.pl</b> - transcation id: %id, amount: %amount', array('%id' => $transaction['id'], '%amount' => uc_currency_format($transaction['amount']/100)));
          
          $user_message = t('<strong>Your transaction has been rejected by Płatności.pl</strong>');
          uc_order_comment_save($order->order_id, $order->uid, $user_message, 'order', 'payment_rejected', true);
          
          uc_order_update_status($order->order_id, uc_order_state_default('payment_rejected'));
        }
        else if($transaction['status'] == PPTS_CANCELLED){
          if($order->order_status != uc_order_state_default('canceled')){
            $message = t('Transaction has been canceled - transaction id: %id, amount: %amount', array('%id' => $transaction['id'], '%amount' => uc_currency_format($transaction['amount']/100)));
            
            $user_message = t('<strong>Transaction has been canceled</strong>');
            uc_order_comment_save($order->order_id, $order->uid, $user_message, 'order', 'canceled', true);
            uc_order_update_status($order->order_id, uc_order_state_default('canceled'));
          }
        }
        else if($transaction['status'] == PPTS_PAYMENT_REJECTED){
          $message = t('Transaction has been rejected - the transaction was canceled after the payment has been received, or there was no way to return the payment automatically. Please contact Płatności.pl - transaction id: %id', array('%id' => $transaction['id']));
          
          $user_message = t('Transaction has been canceled after the payment has been received, or there was no way to return the payment automatically. We will contact you as soon as possible');
          uc_order_comment_save($order->order_id, $order->uid, $user_message, 'order', 'payment_rejected', true);
          
          uc_order_update_status($order->order_id, uc_order_state_default('payment_rejected'));
        }else if($transaction['status'] == PPTS_NEW){
          $message = t('Transaction submitted to Platnosci.pl for processing - transaction id: %id', array('%id' => $transaction['id']));
          uc_order_update_status($order->order_id, uc_order_state_default('payment_pending'));
          $order->payment_details['trans_id'] = $transaction['id'];
          platnosci_pl_extract_payment_data($transaction, $order);
        }
        if($message){
          uc_order_comment_save($order->order_id, $order->uid, $message, 'admin');
        }
        $order->payment_details['ppl_status'] = $transaction['status'];
        platnosci_pl_payment_save($order);
      }
    }
  }
}

function platnosci_pl_extract_payment_data($trans, &$order){
  $pay_type = $order->payment_details['ppl_type'];
  $base = array('id' => '', 'pos_id' => '', 'session_id' => '', 'order_id' => '', 'amount' => '', 'status' => '', 'pay_type' => '', 'pay_gw_name' => '', 'desc' => '', 'desc2' => '', 'create' => '', 'int' => '', 'sent' => '', 'recv' => '', 'cancel' => '', 'auth_fraud' => '', 'ts' => '', 'sig' => '');
  $order->payment_details['data'][$pay_type] = array_diff_key($trans, $base);
}

?>
