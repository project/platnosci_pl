<?php

/* note: 'PPE' stands for 'platnosci.pl Error' */
define('PPE_MISSING_POS_ID',		100);	// brak parametru pos id
define('PPE_MISSING_SESS_ID',		101);	// brak parametru session id
define('PPE_MISSING_TS',			102);	// brak parametru ts
define('PPE_MISSING_SIG',			103);	// brak parametru sig
define('PPE_MISSING_DESC',			104);	// brak parametru desc
define('PPE_MISSING_CLIENT_IP',	105);	// brak parametru client ip
define('PPE_MISSING_FIRSTNAME',	106);	// brak parametru first name
define('PPE_MISSING_LASTNAME',	107);	// brak parametru last name
define('PPE_MISSING_STREET',		108);	// brak parametru street
define('PPE_MISSING_CITY',			109);	// brak parametru city
define('PPE_MISSING_POSTCODE',	110);	// brak parametru post code
define('PPE_MISSING_AMOUNT',		111);	// brak parametru amount
define('PPE_INVALID_ACCOUNT_NUM',112);	// błedny numer konta bankowego
define('PPE_MISSING_EMAIL',		113);	// brak parametru email
define('PPE_MISSING_PHONE',		114);	// brak numeru telefonu
define('PPE_OTHER_TEMP_ERROR',	200);	// inny chwilowy bład
define('PPE_OTHER_TEMP_DB_ERROR',201);	// inny chwilowy bład bazy danych
define('PPE_POS_LOCKED',			202);	// Pos o podanym identyfikatorze jest zablokowany
define('PPE_INVALID_PAYTYPE',		203);	// niedozwolona wartosc pay type dla danego pos id
define('PPE_PAYTYPE_TEMP_LOCK',	204);	// podana metoda płatnosci (wartosc pay type) jest chwilowo zablokowana dla danego pos id, np. przerwa konserwacyjna bramki płatniczej
define('PPE_AMOUNT_BELOW_MIN',	205);	// kwota transakcji mniejsza od wartosci minimalnej
define('PPE_AMOUNT_EXCEEDED_MAX',206);	// kwota transakcji wieksza od wartosci maksymalnej
define('PPE_TOTAL_AMOUNT_EXCEEDED',207);	// przekroczona wartosc wszystkich transakcji dla jednego klienta w ostatnim przedziale czasowym
define('PPE_EXPRESSPAY_INACTIVE',208);	// Pos działa w wariancie ExpressPayment lecz nie nastapiła aktywacja tego wariantu współpracy (czekamy na zgode działu obsługi klienta)
define('PPE_INVALID_POS_KEY', 209); //błedny numer pos_id lub pos_auth_key
define('PPE_NO_SUCH_TRANSACTION',500);	// transakcja nie istnieje
define('PPE_NO_AUTHORIZATION',	501);	// brak autoryzacji dla danej transakcji
define('PPE_ALREADY_STARTED',		502);	// transakcja rozpoczeta wczesniej
define('PPE_ALREADY_AUTHORIZED',	503);	// autoryzacja do transakcji była juz przeprowadzana
define('PPE_ALREADY_CANCELLED',	504);	// transakcja anulowana wczesniej
define('PPE_ALREADY_SENTFORRECV',505);	// transakcja przekazana do odbioru wczesniej
define('PPE_ALREADY_RECIEVED',	506);	// transakcja juz odebrana
define('PPE_MONEY_RETURN_ERROR',	507);	// bład podczas zwrotu srodków do klienta
define('PPE_TRANSACTION_ERROR',	599);	// błedny stan transakcji, np. nie mozna uznac transakcji kilka razy lub inny, prosimy o kontakt
define('PPE_CRITICAL',				999);	// inny bład krytyczny - prosimy o kontakt

function _platnosci_pl_getErrorDesc($error){
	switch($error){
		case PPE_MISSING_POS_ID:			return "brak parametru pos id";
		case PPE_MISSING_SESS_ID:			return "brak parametru session id";
		case PPE_MISSING_TS:					return "brak parametru ts";
		case PPE_MISSING_SIG:				return "brak parametru sig";
		case PPE_MISSING_DESC:				return "brak parametru desc";
		case PPE_MISSING_CLIENT_IP:		return "brak parametru client ip";
		case PPE_MISSING_FIRSTNAME:		return "brak parametru first name";
		case PPE_MISSING_LASTNAME:			return "brak parametru last name";
		case PPE_MISSING_STREET:			return "brak parametru street";
		case PPE_MISSING_CITY:				return "brak parametru city";
		case PPE_MISSING_POSTCODE:			return "brak parametru post code";
		case PPE_MISSING_AMOUNT:			return "brak parametru amount";
		case PPE_INVALID_ACCOUNT_NUM:		return "błedny numer konta bankowego";
		case PPE_MISSING_EMAIL:				return "brak parametru email";
		case PPE_MISSING_PHONE:				return "brak numeru telefonu";
		case PPE_OTHER_TEMP_ERROR:			return "inny chwilowy bład";
		case PPE_OTHER_TEMP_DB_ERROR:		return "inny chwilowy bład bazy danych";
		case PPE_POS_LOCKED:					return "Pos o podanym identyfikatorze jest zablokowany";
		case PPE_INVALID_PAYTYPE:			return "niedozwolona wartosc pay type dla danego pos id";
		case PPE_PAYTYPE_TEMP_LOCK:		return "podana metoda płatnosci (wartosc pay type) jest chwilowo zablokowana dla danego pos id, np. przerwa konserwacyjna bramki płatniczej";
		case PPE_AMOUNT_BELOW_MIN:			return "kwota transakcji mniejsza od wartosci minimalnej";
		case PPE_AMOUNT_EXCEEDED_MAX:		return "kwota transakcji wieksza od wartosci maksymalnej";
		case PPE_TOTAL_AMOUNT_EXCEEDED:	return "przekroczona wartosc wszystkich transakcji dla jednego klienta w ostatnim przedziale czasowym";
		case PPE_EXPRESSPAY_INACTIVE:		return "Pos działa w wariancie ExpressPayment lecz nie nastapiła aktywacja tego wariantu współpracy (czekamy na zgode działu obsługi klienta)";
    case PPE_INVALID_POS_KEY:     return "Błedny numer pos_id lub pos_auth_key";
		case PPE_NO_SUCH_TRANSACTION:		return "transakcja nie istnieje";
		case PPE_NO_AUTHORIZATION:			return "brak autoryzacji dla danej transakcji";
		case PPE_ALREADY_STARTED:			return "transakcja rozpoczeta wczesniej";
		case PPE_ALREADY_AUTHORIZED:		return "autoryzacja do transakcji była juz przeprowadzana";
		case PPE_ALREADY_CANCELLED:		return "transakcja anulowana wczesniej";
		case PPE_ALREADY_SENTFORRECV:		return "transakcja przekazana do odbioru wczesniej";
		case PPE_ALREADY_RECIEVED:			return "transakcja juz odebrana";
		case PPE_MONEY_RETURN_ERROR:		return "bład podczas zwrotu srodków do klienta";
		case PPE_TRANSACTION_ERROR:		return "błedny stan transakcji, np. nie mozna uznac transakcji kilka razy lub inny, prosimy o kontakt";
		case PPE_CRITICAL:					return "inny bład krytyczny - prosimy o kontakt";
		default:									return t('Error %error: no such error',array('%error' => $error));
	}
}

/* Note: 'PPTS' stands for 'platnosci.pl Transaction State' */
define('PPTS_ZERO',			  0);	// status zerowy - domyślny status używany dla nowo utworzonych wpisów.
define('PPTS_NEW',			  1);	// nowa
define('PPTS_CANCELLED',	  2);	// anulowana
define('PPTS_REJECTED',		  3);	// odrzucona
define('PPTS_PENDING',		  4); // rozpoczeta
define('PPTS_AWAITING_RECV', 5);	// oczekuje na odbiór
define('PPTS_PAYMENT_REJECTED',7);	// płatnosc odrzucona, otrzymano srodki ok klienta po wczesniejszym anulowaniu transakcji, lub nie było mozliwosci zwrotu srodków w sposób automatyczny, sytuacje takie beda monitorowane i wyjasniane przez zespół Płatnosci
define('PPTS_FINALIZED',	 99);	// płatnosc odebrana - zakonczona
define('PPTS_ERROR',			888); // błedny status - prosimy o kontakt

/* Note: 'PPPRM' stands for 'platnosci.pl parameter' */
define('PPPRM_POS_ID',			'pos_id');
define('PPPRM_SESSION_ID',		'session_id');
define('PPPRM_TS',				'ts');
define('PPPRM_SIGNATURE',		'sig');
define('PPPRM_TRANSACTION_ID', 'trans_id');
define('PPPRM_TRANSACTION_STATUS', 'transaction_status');
define('PPPRM_PAYMENT_TYPE',	'pay_type');
define('PPPRM_AMOUNT',			'amount');
define('PPPRM_IP',				'client_ip');
define('PPPRM_FIRSTNAME',		'first_name');
define('PPPRM_LASTNAME',		'last_name');
define('PPPRM_EMAIL',			'email');
define('PPPRM_DESCRIPTION',	'desc');
define('PPPRM_AMOUNT_PS',		'amount_ps');
define('PPPRM_AMOUNT_CS',		'amount_cs');
define('PPPRM_ORDER_ID',		'order_id');
define('PPPRM_CREATED',			'create_time');
define('PPPRM_LAST_UPDATE',	'last_update');
define('PPPRM_USER_ID',			'user_id');
define('PPPRM_ID',				'id');
define('PPPRM_STATUS',			'status');
define('PPPRM_ERROR',			'error');

/* Note: 'PPPRC' stands for 'platnosci.pl procedure' */
define('PPPRC_NEW',		'NewPayment');
define('PPPRC_GET',		'Payment/get');
define('PPPRC_CONFIRM', 'Payment/confirm');
define('PPPRC_CENCEL',	'Payment/cancel');


/* Transaction states
1 nowa
2 anulowana
3 odrzucona
4 rozpoczeta
5 oczekuje na odbiór
6 autoryzacja odmowna
7 płatnosc odrzucona, otrzymano srodki ok klienta po wczesniejszym anulowaniu transakcji, lub nie było mozliwosci zwrotu srodków w sposób automatyczny, sytuacje takie beda monitorowane i wyjasniane przez zespół Płatnosci
99 płatnosc odebrana - zakonczona
888 błedny status - prosimy o kontakt
*/


/* error codes
100 brak parametru pos id
101 brak parametru session id
102 brak parametru ts
103 brak parametru sig
104 brak parametru desc
105 brak parametru client ip
106 brak parametru first name
107 brak parametru last name
108 brak parametru street
109 brak parametru city
110 brak parametru post code
111 brak parametru amount
112 błedny numer konta bankowego
113 brak parametru email
114 brak numeru telefonu
200 inny chwilowy bład
201 inny chwilowy bład bazy danych
202 Pos o podanym identyfikatorze jest zablokowany
203 niedozwolona wartosc pay type dla danego pos id
204 podana metoda płatnosci (wartosc pay type) jest chwilowo zablokowana dla danego pos id, np. przerwa konserwacyjna bramki płatniczej
205 kwota transakcji mniejsza od wartosci minimalnej
206 kwota transakcji wieksza od wartosci maksymalnej
207 przekroczona wartosc wszystkich transakcji dla jednego klienta w ostatnim przedziale czasowym
208 Pos działa w wariancie ExpressPayment lecz nie nastapiła aktywacja tego wariantu współpracy (czekamy na zgode działu obsługi klienta)
500 transakcja nie istnieje
501 brak autoryzacji dla danej transakcji
502 transakcja rozpoczeta wczesniej
503 autoryzacja do transakcji była juz przeprowadzana
504 transakcja anulowana wczesniej
505 transakcja przekazana do odbioru wczesniej
506 transakcja juz odebrana
507 bład podczas zwrotu srodków do klienta
599 błedny stan transakcji, np. nie mozna uznac transakcji kilka razy lub inny, prosimy o kontakt
999 inny bład krytyczny - prosimy o kontakt
*/
